#!/usr/bin/env python3

"""
Minor Bioinformatics for the Life Scientists - Diagnostic Genome Analysis

Template for reading in data and writing output data. Running this script
will print the first three lines of the input BED and pileup files and
write the coverage statistics to the file 'd6_output.csv'.

Deliverable 6
-------------
Make changes to all the functions below, following the instructions
preceded with double '##' signs.

    usage:
        python3 deliverable6.py
"""

# METADATA VARIABLES
__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2018.d6.v2"

# IMPORT
import sys
import csv


# FUNCTIONS
def read_data(data_file):
    """ This function reads in data and returns a list containing one
        line per element. """
    ## Open the file given the filename stored in 'data_file'

    ## Store the data in a list

    ## Strip whitespace characters from the end

    ## Return the list containing the data


def save_coverage_statistics(coverage_file, coverage_statistics):
    """ Writes coverage data to a tab-separated file using
        Python's csv library:
        https://docs.python.org/3/library/csv.html#csv.writer """
    # Open the file
    with open(coverage_file, 'w') as csv_file:
        # Create a csv writer object and specify the delimeter
        csv_writer = csv.writer(csv_file, delimiter='\t')
        # Write the list of tuples to the file
        csv_writer.writerows(coverage_statistics)

def calculate_mapping_coverage(coverage_dict):
    """ Function to calculate all coverage statistics on a per-gene basis
        and store this in a list.
        Note: this function is taken from deliverable 5 and slightly modified
    """

    ## Create an empty list that will hold all data to save
    statistics = []

    ## Iterate over all the genes in the coverage_dict getting the gene name
    ## and list with coverage data for that gene (use 'coverage_dict.items()')

        ## Create a counter for the low-coverage positions, and set to 0

        ## Iterate over all coverage values

            ## If coverage value <30

                ## Increment the low-coverage counter by 1

        ## Store the total coverage for this gene

        ## Store the number of bases for this gene

        ## Calculate and store the average coverage for this gene


        ## Put the following elements to a single tuple and append to the
        ## statistics list.
        ##      * Gene name,
        ##      * Total positions (gene length covered)
        ##      * Average Coverage and (use round with one position)
        ##      * low-coverage positions

    ## Return the list of tuples holding the data
    return statistics


######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    # Input data files
    bed_file = 'example.bed'
    pileup_file = 'example.pileup'

    # Input coverage dictionary (d5)
    coverage_dict = {
        "SOB2" : [99, 100, 100, 100, 100, 100, 100, 101, 110, 110, 110, 100,
                  99, 98],
        "NEXN" : [256, 266, 233, 255, 345, 355, 344, 222, 399, 200, 199, 263,
                  234, 133, 165, 176],
        "TCAP" : [50, 51, 55, 23, 43, 23, 33, 24, 53, 24, 30, 33, 37, 37],
        "MYPN" : [52, 37, 22, 86, 58, 20, 10, 9, 2, 3, 1, 93, 51, 88, 77, 25,
                  14, 48, 9, 64, 7, 56, 74, 13],
        "MYBPC3" : [67, 93, 56, 59, 24, 11, 2, 72, 6, 32, 32, 40, 70, 80, 37,
                    42, 98, 26, 41, 73],
        "MYH6" : [38, 51, 67, 17, 29, 47, 67, 89, 62, 34, 41, 53, 67, 50, 46,
                  62, 89, 27, 12, 22, 7, 39, 39, 40, 33, 18, 93, 65, 52, 12,
                  17, 14, 90, 79, 37],
        "TXNRD2" : [73, 256, 156, 80, 11, 313, 180, 338, 78, 189, 205, 151,
                    29, 48, 286, 38, 62, 208, 134, 257, 118, 44]
    }

    ### OUTPUT ###
    # Output data files
    csv_file = 'd6_output.csv'

    # Read BED data
    print("Reading BED data from", bed_file)
    bed_data = read_data(bed_file)
    if bed_data is None:
        print("No BED-data read, please fix the 'read_data' function...")
    elif len(bed_data) < 10:
        print("Only ", len(bed_data), " lines read, is this the whole file?", sep="")
    else:
        print('A total of', len(bed_data), 'lines have been read. The first 3 lines are:\n\t')
        print('\t', '\n\t'.join(bed_data[0:3]), '\n', sep='')

    # Read Pileup data
    print("\nReading pileup data from", pileup_file)
    pileup_data = read_data(pileup_file)
    if pileup_data is None:
        print("No Pileup-data read, please fix the 'read_data' function...")
    elif len(pileup_data) < 10:
        print("Only ", len(pileup_data), " lines read, is this the whole file?", sep="")
    else:
        print('A total of', len(pileup_data), 'lines have been read. The first 3 lines are:\n\t')
        print('\t', '\n\t'.join(pileup_data[0:3]), '\n', sep='')

    # Store calculated data
    coverage_statistics = calculate_mapping_coverage(coverage_dict)
    
    # Check and write output data
    if not len(coverage_statistics) > 0:
        print("\nNo coverage data written; please fix the 'calculate_mapping_coverage' function.")
    else:
        save_coverage_statistics(csv_file, coverage_statistics)
        print("\nOutput coverage statistics have been written to: ", csv_file, sep="")

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
