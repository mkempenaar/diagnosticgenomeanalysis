#!/usr/bin/env python3

"""
Minor Bioinformatics for the Life Scientists - Diagnostic Genome Analysis

Template for parsing ANNOVAR data

Deliverable 9
-------------
Make changes to the 'parse_annovar_data' function, following the instructions
preceded with double '##' signs. Note that the indentation of these instructions
can be used for placing the code.

    usage:
        python3 deliverable9.py [annovar.txt]

    arguments:
        annovar.txt: the input txt file, output from the ANNOVAR tool

"""

# METADATA VARIABLES
__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2018.d9.v2"

# IMPORT
import sys
import pandas as pd

def parse_annovar_data(anno_input_file):
    ''' This function reads the input ANNOVAR file and returns the data as
    a Pandas DataFrame object, with several changes to columns necessary
    for filtering '''

    ## Use the pandas 'read_table' function to read in the ANNOVAR file

    ## Add a prefix character to the 1000-genomes columns

    ## Split the PolyPhen2 columns and store as new columns

    ## Replace the 'pass' statement with a return of the DataFrame
    pass

######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    # Try to read input argument from the commandline. Use default if
    ## they are missing.
    if len(args) > 1:
        anno_file = args[1]
    else:
        print('Warning, no arguments given, using default value (testing only)...')
        anno_file = 'example.txt'

    # Process the ANNOVAR-file
    annovar = parse_annovar_data(anno_file)
    if (type(annovar) == pd.core.frame.DataFrame):
        # Print information about the DataFrame
        print('''
        The following listing shows all columns in the DataFrame, the number of
        elements that is not-null (i.e., has contents) and the type of contents.
        'int64' = integer, 'float64' is float and 'object' is a String.
        
        Check that the 1000-genomes columns are prefixed with a letter and that the
        PolyPhen2 columns are correctly splitted with the '_value' column set
        to 'float64'.
        ''')
        print(annovar.info())
    else:
        print("Warning, the returned object is *not* a Pandas DataFrame!")
        return 1

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
