#!/usr/bin/env python3

"""
Minor Bioinformatics for the Life Scientists - Diagnostic Genome Analysis

Template for parsing and filtering ANNOVAR data

Deliverable 9
-------------
Make changes to the 'parse_annovar_data' function, following the instructions
preceded with double '##' signs. Note that the indentation of these instructions
can be used for placing the code.

    usage:
        python3 deliverable9.py [annovar.txt]

    arguments:
        annovar.txt: the input txt file, output from the ANNOVAR tool

"""

# METADATA VARIABLES
__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2018.d9.v1"

# IMPORT
import sys
from operator import itemgetter

## Enter the column numbers that need to be extracted from each line
COLUMNS = []

def parse_annovar_data(anno_input_file):
    ''' This function reads the input ANNOVAR file line by line, skipping the first
    header line. The remaining lines are parsed to filter out variants in exons
    with their annotation '''

    ## Open the annovar file
    
    ## Skip the first line using the 'next()' function

    ## For every line (variant) in the file

        ## Split the line (use the tab '\t')

        ## 'Extract' the values of interest using the 'itemgetter' or a list-comprehension

        ## Print a line with general information:
        ##      gene name, chromosome, reference base, etc.
        ## Print a line, starting with a '\t' followed by the
        ## required annotation data
        ## Print a blank line ('\n')

    ## Close the file

######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    # Try to read input argument from the commandline. Use default if
    ## they are missing.
    if len(args) > 1:
        anno_file = args[1]
    else:
        print('Warning, no arguments given, using default value (testing only)...')
        anno_file = 'example.txt'

    # Process the ANNOVAR-file
    parse_annovar_data(anno_file)

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
