#!/usr/bin/env python3

"""
Minor Bioinformatics for the Life Scientists - Diagnostic Genome Analysis

Simple template for parsing BED data.

Deliverable 2
-------------
Make changes to the 'parse_bed_data' function, following the instructions
preceded with double '##' signs. Note that the indentation of these instructions
can be used for placing the code.

    usage:
        python3 deliverable2.py
"""

# METADATA VARIABLES
__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2019.d2.v1"

# IMPORT
import sys

# FUNCTIONS
def parse_bed_data(bed_data):
    """ Function that parses BED data and stores its contents
        in a dictionary
    """

    ## Remove this print statement after the first time executing this program
    print('Input data: ', bed_data)

    ## Create empty dictionary to hold the data
    bed_dict = {}

    ## Iterate over all lines in the 'bed_data' list

        ## Split the line based on the used separator

        ## Change the 'data-type' to integers for the start- and stop-coordinates

        ## Check if the chromosome is already present in the bed_dict

            ## If True, append the start, stop and name as a tuple

            ## If False, add a new key using the chromosome and a list
            ## containing a tuple with the start, stop and name as value

    ## Return the bed_dict once all lines are done
    return bed_dict


######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function that tests for correct parsing of BED data """
    ### INPUT ###
    bed_data = [
        "1	237729847	237730095	RYR2",
        "1	237732425	237732639	RYR2",
        "1	237753073	237753321	RYR2",
        "18	28651551	28651827	DSC2",
        "18	28654629	28654893	DSC2",
        "18	28659793	28659975	DSC2",
        "X	153648351	153648623	TAZ",
        "X	153648977	153649094	TAZ",
        "X	153649222	153649363	TAZ"
    ]

    ### OUTPUT ###
    expected_bed_dict = {
        '1':  [(237729847, 237730095, 'RYR2'),
               (237732425, 237732639, 'RYR2'),
               (237753073, 237753321, 'RYR2')],
        '18': [(28651551, 28651827, 'DSC2'),
               (28654629, 28654893, 'DSC2'),
               (28659793, 28659975, 'DSC2')],
        'X':  [(153648351, 153648623, 'TAZ'),
               (153648977, 153649094, 'TAZ'),
               (153649222, 153649363, 'TAZ')]}

    # Call the parse-function
    bed_dict = parse_bed_data(bed_data)
    _assert_output_vs_expected(bed_dict, expected_bed_dict)

def _assert_output_vs_expected(output, expected):
    """ Compares given output with expected output.
    Do not modify. """
    import unittest
    if isinstance(output, dict):
        testcase = unittest.TestCase('__init__')
        try:
            testcase.assertDictEqual(expected, output,
                                     msg="\n\nPlease review the above comparison and change your code")
        except AssertionError as error:
            print('\nUnfortunately, the output is *not* correct. Below is the comparison between ' +
                  'the expected output and your output.')
            print(error)
            return 0
        print("\nWell done! Output is correct!")
    else:
        print("\n\nUnfortunately, the output is *not* a dictionary!")

if __name__ == '__main__':
    sys.exit(main(sys.argv))
