{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6 Analysis Step #4; Finding Variants using `Varscan`\n",
    "\n",
    "Most likely you've already seen how a true variant (not a single or few mismatches) looks when we looked at the data in the IGV-browser. For such a variant you see a number of reads having a different base at that positions compared to the reference genome. As we've mentioned before, we are interested in all these variants in our patient data for the 55 cardiopanel genes. The previous *visualize-your-data* assignment did not ask to find all these variants using the IGV-browser because in this section we are going to use a program to do this for us.\n",
    "\n",
    "The tool that we will use is the [Varscan 2](http://genome.cshlp.org/content/22/3/568) tool that scans the pileup file for variants. This tool has a number of settings that, combined, defines when a position is called a variant. You could do this in a naive way and just report for each position if there is a change. But this will result in many false positive variants (can you explain this?). We need a more statistical approach to filter out low quality variants and that's why the tool has settings to for instance set the minimum amount of reads (coverage) at a position to consider looking for variations. Also the variants itself should be supported by a minimum number of reads and the base quality of that position should not be too low.\n",
    "\n",
    "<!-- \n",
    "http://genome.cshlp.org/content/22/3/568\n",
    "http://varscan.sourceforge.net/using-varscan.html\n",
    "-->\n",
    "\n",
    "From the toolbox select the <strong>Varscan</strong> tool, select your pileup file as input and leave all the settings to their defaults and execute the tool (this will take 5 to 10 minutes to complete as it has to analyze 100 million lines of data):\n",
    "<img src=\"pics/varScan.png\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"pics/varScan_output.png\" style=\"float: right\">\n",
    "\n",
    "### VCF File Filtering\n",
    "The result of the Varscan tool is a single new file in your history in the [**vcf**](http://www.internationalgenome.org/wiki/Analysis/vcf4.0/) file format where each line describes a single variant. In Galaxy you can then directly see how many variants you have; over 7000 in the example shown to the right. Note the 24 comment-lines at the top of the file.\n",
    "\n",
    "When looking at this file in more detail it is fairly easy to see variants in places we are not interested in. As you've seen in IGV, many regions have been sequenced outside of our genes of interest, or even very far from a gene at all. Varscan also checked those regions for variants and this assignment asks to filter the list of variants only keeping those within exon boundaries of our genes of interest. Then, another filtering step is filtering on actual variants. We do this by looking at the [allele frequency](https://en.wikipedia.org/wiki/Allele_frequency) value included in the VCF file. In our case, this value describes the percentage of reads having the **variant** base.\n",
    "\n",
    "### Assignment 8; VCF File Processing\n",
    "\n",
    "Please briefly read the linked wikipedia page to understand why this value is of importance. As we are working with patient data, we use the protocol as described by the UMCG that states that variants with a minimum frequency of **30%** are retained. This means we will filter out any variants with a *lower* value. \n",
    "\n",
    "There are once again step-by-step instructions for completing this assignment. It is however possible to do it without these instructions as we partly repeat steps we've taken in the previous assignments.\n",
    "\n",
    "1. Read in the data into a data-frame using the `read.delim` function\n",
    "    * Make sure the data has a proper header (line 24 in the data)\n",
    "    * Provide the `stringsAsFactors = FALSE` argument, otherwise the next part won't work (we cannot split an R `factor`)\n",
    "\n",
    "Now that we have the data in a data-frame, inspect that everything is loaded correctly. The first thing that we'll do now is get the frequency value for each variant. Read the comment lines in the VCF file to see how this value is stored. You'll see that the columns themselves contain more fields, separated by a colon (`:`) which we can use to *split* the data to get to the value we want.\n",
    "\n",
    "2. Split the column containing the frequency value using the `strsplit` function\n",
    "\n",
    "The output of this function is a list in which each item is a vector with the separate items resulting from the split (inspect this object in RStudio). To get the frequency value from this list, here is some R magic that converts the result from `strsplit` into a data-frame (found on [Stackoverflow](https://stackoverflow.com/questions/20428742/select-first-element-of-nested-list)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "do.call(rbind, splitted_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What it does is `rbind` all vectors into a data-frame, but we haven't seen (and probably won't see) the `do.call` function before (pure magic). Again, inspect this data-frame to see where the actual value is located that you want. Also note the data type of this column, is it numeric?\n",
    "\n",
    "3. Write a so called one-liner (multiple statements in a single line of code) that:\n",
    "    * replaces the `%` sign with nothing (an empty string; `''`) using the `gsub` function\n",
    "    * converts the data type of this column to `numeric`\n",
    "    * saves only this column into a new variable\n",
    "\n",
    "We now have the frequency value available for filtering and we'll do that by adding it to the `GRanges` object we create next:\n",
    "\n",
    "4. Create a `GRanges` object as we've done for assignment 3 by simply creating an `IRanges` object where the `start` and `end` parameters both get the variant position column. Also provide the `seqnames` parameter to `GRanges` which get the contents of the chromosome column.\n",
    "\n",
    "`GRanges` objects can contain other data as well, called *associated metadata*. Using the `mcols` function we can see existing or assign data to each variant. Adding this as a data-frame allows us to set a name for this column as demonstrated below with an example from the previous chapter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "GRanges object with 5 ranges and 0 metadata columns:\n",
       "       seqnames              ranges strand\n",
       "          <Rle>           <IRanges>  <Rle>\n",
       "  SOD2     chr6 160103505-160103690      *\n",
       "  SOD2     chr6 160105866-160106085      *\n",
       "  SOD2     chr6 160109138-160109294      *\n",
       "  SOD2     chr6 160113673-160113915      *\n",
       "  SOD2     chr6 160114157-160114219      *\n",
       "  -------\n",
       "  seqinfo: 18 sequences from an unspecified genome; no seqlengths"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Select and show data for a single gene (note that it specifies \"... and 0 metadata columns\")\n",
    "# ('granges' object is used from assignment 3)\n",
    "granges"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "GRanges object with 5 ranges and 1 metadata column:\n",
       "       seqnames              ranges strand |    GCperc\n",
       "          <Rle>           <IRanges>  <Rle> | <numeric>\n",
       "  SOD2     chr6 160103505-160103690      * |      45.7\n",
       "  SOD2     chr6 160105866-160106085      * |      51.6\n",
       "  SOD2     chr6 160109138-160109294      * |      45.3\n",
       "  SOD2     chr6 160113673-160113915      * |      38.3\n",
       "  SOD2     chr6 160114157-160114219      * |      44.3\n",
       "  -------\n",
       "  seqinfo: 18 sequences from an unspecified genome; no seqlengths"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Generate some random values for 'gc-percentage'\n",
    "gc_percentage <- round(rnorm(5, mean = 50, sd = 10), 1)\n",
    "\n",
    "# Bind this data to the 'GRanges' object\n",
    "mcols(granges) <- DataFrame(GCperc=gc_percentage)\n",
    "\n",
    "granges"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the example above to associate the frequency value with the created `GRanges` object. Now that we have both the BED-data (in a `GRangesList` object) and the VCF data in a `GRanges` object, we can \n",
    "\n",
    "5. Get all variants that fall within an exon\n",
    "    * Use the `findOverlaps` function and store its output\n",
    "    * Convert the output to a data-frame\n",
    "    \n",
    "The result from `findOverlaps` contains two columns of which we are interested in the `subjectHits` column; the rows from the VCF data that lie within an exon. You can use this column to subset the `GRanges` VCF object with. Note that the `queryHits` column describes not the exons but the genes; meaning a queryHit value of `1` refers to a hit within any of the `ABCC9` gene exons.\n",
    "\n",
    "6. Filter the **remaining** variants based on their frequency, using a minimum of **30%**\n",
    "    * Instead of removing rows or creating another subset, make sure you know which rows are to be kept after both filtering steps as we are going to reconstruct the original VCF file (we need it in Galaxy)\n",
    "    * Manually sample a few rows to see if they do fall within an exon and have a frequency > 30%\n",
    "\n",
    "\n",
    "7. Now that you have all the row numbers of variants that we want to keep, you need to think of a way to re-construct the VCF file and save it to disk.\n",
    "    * Hint: read in the VCF file again for subsetting using the `readLines` function\n",
    "    * Note: do not forget to include the 24 header lines!\n",
    "\n",
    "\n",
    "8. Upload the new VCF file into Galaxy as we'll use it in the next chapter."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Assignment 9; Variant Visualization\n",
    "\n",
    "We will create two simple visualizations:\n",
    "\n",
    "* Visualize the **allele frequency** for all remaining variants using the `hist` function. Remember that you can use the `mcols` function to get a metadata column or use the `$`-sign and the name of the column. Pass the argument `breaks=20` to `hist` and answer the following question in your lab-journal:\n",
    "    - Can you explain the two peaks that you see in your histogram (around 50% and 100%)? \n",
    "* Visualize the amount of variants per gene. A simple solution for getting the numbers is to use the `table` function on the `queryHits` column (output of `findOverlaps`). Convert it to a data frame and then use the gene indices to get the actual gene names. Now you should have a combination of gene and number of variants. Create a barplot with this data as in the previous chapter and use `cex.names=0.7` to scale the gene names to make them all visible."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.6.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
