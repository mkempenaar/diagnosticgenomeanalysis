# Material for the Diagnostic Genome Analysis course

Below is a listing of all chapters and extra documents used for the course.

## Chapters & Manuals

* ### [Course Introduction](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/00_introduction.ipynb)

    **Course Description and Background Information**  

* ### [Chapter 1; Galaxy Introduction](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/01_galaxy_introduction.ipynb)

    **Introducing the Galaxy Server(s)**  

* ### [Chapter 2; Data Introduction](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/02_data.ipynb)

    **Explanation of the FASTQ Data Format**

* ### [Chapter 3; Quality Control](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/03_qc.ipynb)

    **Performing Quality Control on NGS Data**

* ### [Chapter 4; Read Mapping](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/04_read_mapping.ipynb)

    **Mapping NGS Data to a Reference Genome**

* ### [Chapter 5; Pileup](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/05_pileup.ipynb)

    **Inspecting the Mapping Data**

* ### [Chapter 6; Varscan](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/06_varscan.ipynb)

    **Finding the Actual Variant Positions**

* ### [Chapter 7; Annovar](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/07_annovar.ipynb)

    **Annotating found Variants using ANNOVAR**

## Other Documentation

* ### [Forking and Mananaging a Repository](http://nbviewer.jupyter.org/urls/bitbucket.org/mkempenaar/diagnosticgenomeanalysis/raw/bfvh15diagn/chapters/a2_repository.ipynb)

    **Instructions on Forking this Repository**


## Preliminary Course Planning

* Week 1: *Course Introduction* & Chapters **1** through **3.2**
* Week 2: Chapters **3** through **5**
* Week 3: Remaining programming assignments chapter **5** and running the `Varscan` Galaxy tool from chapter **6**
* Week 4: ~~Programming the `Varscan` VCF-filter application (deliverable 8), creating a Galaxy VCF-filter tool (updated instructions) and running the `ANNOVAR` tool from chapter **7**~~
* Week 5: ~~Describing the `ANNOVAR` output and processing this using Python (deliverable 9)~~
* Week 6: ~~Continuing programming assignments; filtering `RefSeq_Gene` data (deliverable 10) and creating your own template for deliverable 11.~~
* Week 7: ~~Finishing deliverable 11, summarizing the results and creating a Poster (chapter **7.4**)~~